package cz.cvut.fit.bi.tjv.main;

import cz.cvut.fit.bi.tjv.model.Customer;
import cz.cvut.fit.bi.tjv.model.Product;
import cz.cvut.fit.bi.tjv.service.CustomerService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import cz.cvut.fit.bi.tjv.service.ProductService;

import java.util.Arrays;

public class SpringOrmMain {
    public static void main(String[] args) {

       
        //Create Spring application context
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

        ctx.close();
    }
}